package sheridan; 

import java.math.BigInteger;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
* Divjot Chawla- 991505770
*/
public class PrimeNumber {

  static Logger logger = Logger.getLogger(PrimeNumber.class.getName()); 
	
	/**
 * @param args updated
 */
  public static void main(String[] args) {
    printTest(10, 4);
    printTest(2, 2);
    printTest(54161329, 4);
    printTest(1882341361, 2);
    printTest(36, 9);

   
    logger.log(Level.INFO, isPrime(54161329) + " expect false");
    logger.log(Level.INFO,isPrime(1882341361) + " expect true");
    logger.log(Level.INFO,isPrime(2) + " expect true");
	
    int numPrimes = 0;
    for(int i = 2; i < 10000000; i++) {
      if(isPrime(i)) {
        numPrimes++;
		}
	}

    boolean[] primes = getPrimes(10000000);
   int np = 0;
    for(boolean b : primes) {
      if(b) { 
        np++;
			}}

    logger.log(Level.INFO, (new BigInteger(1024, 10, new Random()).toString()));
}

  public static boolean[] getPrimes(int max) {
    boolean[] result = new boolean[max + 1];
    for(int i = 2; i < result.length; i++) {
      result[i] = true;
    }
    final double LIMIT = Math.sqrt(max);
   
    for(int i1 = 2; i1 <= LIMIT; i1++) {
      if(result[i1]) {
        int index = 2 * i1;
        while(index < result.length){
          result[index] = false;
          index += i1;
			}
		}
	}
    return result;
}


  public static void printTest(int num, int expectedFactors) {
    int actualFactors = numFactors(num);
    logger.log(Level.INFO,"Testing " + num + " expect " + expectedFactors + ", " +
			"actual " + actualFactors);

    if(actualFactors == expectedFactors) {
    	logger.log(Level.INFO,"PASSED");
    }
	    else {
	    logger.log(Level.INFO,"FAILED");
	    }

}


  public static boolean isPrime(int num) {
	  if(num >= 2) 
		{
      final double LIMIT = Math.sqrt(num);
      boolean isPrime = false;
      if(num == 2) {
        isPrime = true;
		}else {
        isPrime = false;
		}
      int div = 3;
      while(div <= LIMIT && isPrime) {
        isPrime = num % div != 0;
        div += 2;
	}
      return isPrime; 
		}else
		{
			throw new IllegalArgumentException("failed precondition. num must be >= 2. num: " + num);
		}
		}


  public static int numFactors(int num) {
	  if (num >= 2)
		{
      int result = 0;
      final double SQRT = Math.sqrt(num);
      for(int i = 1; i < SQRT; i++) {
        if(num % i == 0) {
        	result += 2;
		}
	}
      if(num % SQRT == 0) {
        result++;
    }
      return result;
}
  else
	{
      throw new IllegalArgumentException("failed precondition. num must be >= 2. num: " + num); 
	}
  }
}